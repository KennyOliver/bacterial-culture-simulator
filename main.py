import math
import random
import time
#====================
# MAIN PROGRAM

print("<-- Bacterial Culture Simulator -->")
print("This Python program simulates a basic colony of bacteria.")

print("-" * 30)

initial_population = int(input("Enter inital population size\n\t--> "))
growth_rate = float(input("Enter the growth/death rate (eg. 1.02)\n\t--> "))
simulation_duration = int(input("Enter the duration of the simulation (seconds)\n\t--> "))

print("-" * 30)

print("<-- Parameters provided -->")
print(f"Initial size: {initial_population}")
print(f"Growth rate: {growth_rate * 100}%")
print(f"Simulation duration: {simulation_duration}s")

print("-" * 30)

population = initial_population

for _ in range(simulation_duration):
  print(f"Count: {math.floor(population)}")
  population *= growth_rate + random.randint(1,10) / 100
  time.sleep(1)

'''print("-" * 30)

calculate_growth = ((population - initial_population) / initial_population) * 100

print(f"Your bacterial culture grew about {calculate_growth:.2f}%")'''