# bacterial-culture-simulator :petri_dish:

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/bacterial-culture-simulator/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/bacterial-culture-simulator?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/bacterial-culture-simulator?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/bacterial-culture-simulator?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/bacterial-culture-simulator)](https://replit.com/@KennyOliver/bacterial-culture-simulator)

**Simulate a bacterial culture population with environmental conditions that you can control!**

---
Kenny Oliver ©2021
